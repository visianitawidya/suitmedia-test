import { useState } from 'react';
import {
    FormControl,
    FormLabel,
    Input,
    FormErrorMessage,
    Button,
} from '@chakra-ui/react';

function Form() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const handleNameChange = (e) => setName(e.target.value);
    const handleEmailChange = (e) => setEmail(e.target.value);
    const handleMessageChange = (e) => setMessage(e.target.value);

    const isNameError = name === '';
    const isEmailError = email === '';
    const isMessageError = message === '';

    const isEmailValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

    return (
    <form >
        <FormControl isInvalid={isNameError}>
        <FormLabel>Name</FormLabel>
        <Input type="text" value={name} onChange={handleNameChange} />
        {isNameError && <FormErrorMessage>This field is required.</FormErrorMessage>}
        </FormControl>

        <FormControl mt={4} isInvalid={isEmailError || !isEmailValid}>
        <FormLabel>Email</FormLabel>
        <Input type="email" value={email} onChange={handleEmailChange} />
        {isEmailError && <FormErrorMessage>This field is required.</FormErrorMessage>}
        {!isEmailError && !isEmailValid && (
            <FormErrorMessage>Invalid email address.</FormErrorMessage>
        )}
        </FormControl>

        <FormControl mt={4} isInvalid={isMessageError}>
        <FormLabel>Message</FormLabel>
        <Input
            as="textarea"
            rows={4}
            value={message}
            onChange={handleMessageChange}
        />
        {isMessageError && <FormErrorMessage>This field is required.</FormErrorMessage>}
        </FormControl>

        <Button w="full" mt={4} bg="#6C95C0" color="white" disabled={isNameError || isEmailError || !isEmailValid || isMessageError}>
        SUBMIT
        </Button>
    </form>
    );
}

export default Form;
