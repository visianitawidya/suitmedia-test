import { useState } from "react";
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons'
import { Link } from "@chakra-ui/react";
import './Dropdown.css';

const Navbar = () => {
  const [open, setOpen] = useState(false);
  const [dropdown, setDropdown] = useState(false);

  return (
    <nav className="bg-white">
      <div className="flex items-center font-medium justify-around">
        <div className="z-50 p-5 md:w-auto w-full flex justify-between">
          <h2 className="md:cursor-pointer font-bold text-2xl">Company</h2>
          <div className="text-3xl md:hidden" onClick={() => setOpen(!open)}>
            { open ? <CloseIcon/> : <HamburgerIcon/> }
          </div>
        </div>
        <ul className="md:flex hidden uppercase items-center text-gray-600 gap-8">
            <li
              onMouseOver={() => setDropdown(true)}
              onMouseOut={() => setDropdown(false)}
            >
                <Link>
                  ABOUT
                </Link>
                {dropdown && (
                  <ul
                  className='dropdown-menu'
                  >
                    <li className="dropdown-link">HISTORY</li>
                    <li className="dropdown-link">VISION MISSION</li>
                  </ul>
                )}
            </li>
            <li>
                OUR WORK
            </li>
            <li>
                OUR TEAM
            </li>
            <li>
                CONTACT
            </li>
            
        </ul>
        {/* Mobile nav */}
        <ul
          className={`
        md:hidden bg-white fixed w-full top-0 overflow-y-auto bottom-0 py-24 pl-10
        duration-500 z-10 text-left text-lg ${open ? "left-0" : "left-[-100%]"}
        `}
        >
          <li className={`my-5 ${dropdown ? 'mb-20' : ''}`}
              onMouseOver={() => setDropdown(true)}
              onMouseOut={() => setDropdown(false)}>
              <Link>
              ABOUT
              </Link>
              {dropdown && (
                  <ul
                  className='dropdown-menu z-20 mt-24'
                  >
                    <li className="dropdown-link">HISTORY</li>
                    <li className="dropdown-link">VISION MISSION</li>
                  </ul>
                )}
          </li>

          <li className="my-5">
              OUR WORK
          </li>

          <li className="my-5">
              OUR TEAM
          </li>
          <li className="my-5">
              CONTACT
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;