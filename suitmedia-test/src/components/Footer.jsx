import FBLogo from '../assets/facebook-official.png';
import TwitterLogo from '../assets/twitter.png';


const Footer = () => {
    return (
        <footer className="bg-zinc-800 py-4">
        <div className="container mx-auto text-center">
            <div className="mb-4">
            <p className="text-gray-300 text-sm">
                Copyright &copy; 2016. PT Company
            </p>
            </div>
            <div className="flex justify-center">
            <div className="mr-6">
                <img
                src={FBLogo}
                alt="Logo Facebook"
                className="h-4"
                />
            </div>
            <div>
                <img
                src={TwitterLogo}
                alt="Logo Twitter"
                className="h-4"
                />
            </div>
            </div>
        </div>
        </footer>
    );
};

export default Footer;
