const Card = ({ bgColor, title, text, logoSrc }) => {

    const cardStyles = {
        backgroundColor: `#${bgColor}`,
        };

    return (
        <>
        <div className="p-6 min-h-64 max-w-80 shadow-md text-white flex flex-col items-center justify-center" style={cardStyles}>
            <img
                src={logoSrc}
                alt="Logo"
                className="h-8"
            />
            <h2 className="text-lg font-bold my-2">{title}</h2>
            <p className="mt-5">{text}</p>
        </div>
        </>
    );
};

export default Card;