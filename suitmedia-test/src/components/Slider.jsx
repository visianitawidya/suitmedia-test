import { useState } from 'react';
import { BiChevronLeftCircle, BiChevronRightCircle } from 'react-icons/bi';
import { RxDotFilled } from 'react-icons/rx';
import image1 from '../assets/about-bg.jpg'
import image2 from '../assets/bg.jpg'

const Slider = () => {
    const slides = [image1,image2];
    const text = ["WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM", "THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY"]
    const [currentIndex, setCurrentIndex] = useState(0);

    const prevSlide = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
        setCurrentIndex(newIndex);
    };

    const nextSlide = () => {
        const isLastSlide = currentIndex === slides.length - 1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    };

    const goToSlide = (slideIndex) => {
        setCurrentIndex(slideIndex);
    };

    return (
        <>
            <div className="relative">
                <div
                    className="bg-cover bg-center w-full h-[36rem] duration-700"
                    style={{ backgroundImage: `url(${slides[currentIndex]})` }}
                >
                    <div className="absolute bottom-1/4 left-[15%] w-[50%]">
                        <div className="bg-black bg-opacity-50 p-2">
                            <p className='text-left md:text-xl sm:text-lg font-semibold text-white duration-700'>{text[currentIndex]}</p>
                        </div>
                    </div>
                    <div className="absolute left-0 top-1/2 transform -translate-y-1/2 mx-4">
                        <div className="text-gray-200 rounded-full flex items-center justify-center">
                            <BiChevronLeftCircle onClick={prevSlide} size={40} /> 
                        </div>
                    </div>
                    <div className="absolute right-0 top-1/2 transform -translate-y-1/2 mx-4">
                        <div className="text-gray-200 rounded-full flex items-center justify-center">
                            <BiChevronRightCircle onClick={nextSlide} size={40} />
                        </div>
                    </div> 
                    <div className="absolute inset-x-1/2 bottom-0">
                        <div className='flex justify-center'>
                            {slides.map((slide, slideIndex) => (
                            <div
                                key={slideIndex}
                                onClick={() => goToSlide(slideIndex)}
                                className='text-2xl cursor-pointer'
                            >
                                <RxDotFilled className={`h-10 w-10 ${ slideIndex === currentIndex ? 'text-gray-200' : 'text-gray-400'}`} />
                            </div>
                            ))}
                        </div> 
                    </div>
                </div>
                </div>
        </>
        
    );
}

export default Slider;