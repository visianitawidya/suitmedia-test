import './App.css';
import Slider from './components/Slider';
import Navbar from './components/Navbar';
import Card from './components/Card';
import Footer from './components/Footer';
import Form from './components/Form';
import image1 from './assets/innovative.png'
import image2 from './assets/loyalty.png'
import image3 from './assets/respect.png'

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Slider/>
      <div className='mt-12 mb-20'>
        <h1 className='font-bold text-xl'>OUR VALUES</h1>
        <div className='grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-3 grid-flow-row justify-center md:mx-52 sm: mx-12 my-4 gap-4'>
          <Card bgColor="d77e72" title="INNOVATIVE" logoSrc={image1} text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime exercitationem dolorem deserunt, unde, eaque ipsa?" />
          <Card bgColor="759971" title="LOYALTY" logoSrc={image2} text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit similique eum itaque facere temporibus dolores." />
          <Card bgColor="6C95C0" title="RESPECT" logoSrc={image3} text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, sit? Tenetur et neque quod incidunt!" />
        </div>
      </div>
      <div className='my-12'>
        <h1 className='font-bold text-xl'>CONTACT US</h1>
        <div className='bg-blue md:mx-52 sm: mx-12 my-4'>
          <Form/>
        </div>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
